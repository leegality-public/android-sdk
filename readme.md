# Leegality Android SDK 4.8

### Steps to Use ###

1. Download Leegality Library from https://gitlab.com/leegality-public/android-sdk/-/raw/master/leegality.aar
2. Import Leegality Library to your project by following the steps laid down in https://developer.android.com/studio/projects/android-library.html#AddDependency. 
3. To initialize signing, start activity Leegality provided by the Leegality Library. 
Pass signing URL in url key.
    ```
    Intent intent = new Intent(getApplicationContext(), Leegality.class);
    intent.putExtra("url", "signing-url-here");
    startActivityForResult(intent, REQUEST_CODE);
    //REQUEST_CODE is your constant, which is used to identify particular activity.
    ```
    To customise the duration for which the success screen on the URL remains open before automatically closing down, you can append the URL with the 
    query parameter “timer”.  The default value is 5 and you can pass any integer value between 0 to 60.  For example, to set the duration at 0 
    seconds, pass the timer parameter with value 0 as follows: 

    https://app1.leegality.com/sign/ea23fced-36e4-4fe3-ae32-181a09c2f13a?timer=0
 


4. To disable zoom feature you need to pass the zoom parameter as false in intent. By default its enabled.
   ```
   intent.putExtra("zoom", false);
   ```
5. Leegality SDK broadcast events related to user activity. If you want to receive these broadcasts you can use following code.
   ```
   IntentFilter filter = new IntentFilter("com.gspl.leegality.events"); 
   registerReceiver(new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            Log.d("test", intent.getExtras().getString("data"));
        }
    }, filter);
    ```
### Note: 

1. It is recommended to start signing process by using startActivityForResult instead of startActivity to receive the response/error. 
2. If you are using captureLocation, capturePhoto parameter in API. You have to take following permission from the user. These are runtime permissions if you dont take following permission Leegality will not be able to capture Location & Photo of user.
    ```
    <uses-permission android:name="android.permission.CAMERA" />
    <uses-permission android:name="android.permission.ACCESS_FINE_LOCATION" />
    ```
3. If you are using captureLocation, capturePhoto parameter in API. Your android application should be atleast at version 21.
4. If you want to autofill Leegality OTP and NSDL/CDAC OTP at the signing page. You have to take following permission from the user. These are runtime permission if you dont take following permission user have to fill all OTP's manually.
    ```
    <uses-permission android:name="android.permission.READ_SMS" />
    <uses-permission android:name="android.permission.RECEIVE_SMS" />
    ```
5. Please also take the following runtime permission from the user to allow downloading of the document.
    ```
    <uses-permission android:name="android.permission.WRITE_EXTERNAL_STORAGE" />
    ```

### Result: ###

Override method onActivityResult to check the response of signing process.

1. In case of successfully completion of signing process, signing window will be closed automatically and you will get a response with key name 'message' and corresponding 'success message' in the Intent.
2. In case of error in signing process, signing window will be closed automatically and you will get a response with key name 'error' and corresponding 'error message'.

```
@Override
protected void onActivityResult(int requestCode, int resultCode, Intent data){
	//Check requestCode, If it match with the provided REQUEST_CODE then this response is for the signing process.	
	if(requestCode == REQUEST_CODE){
		String error = data.hasExtra("error") ? data.getExtras().getString("error") : null;
		String message = data.hasExtra("message") ? data.getExtras().getString("message") : null;
		if(error != null){		
			log.info("Error: ", error);
		}else if(message != null){
			log.info("Message: ", message);
		}		
	}
	super.onActivityResult(requestCode, resultCode, data);
}
```

### JSON Structure: ###

Broadcast event JSON structure:

```
{
	"invitationId":"string",
	"eventName":"string",
	"sessionId":"string",
	"data":{
		"notificationRaised":{
			"messages":[
			{
				"code":"string",
				"message":"string"
			}],
		"type":"string"
		}
	}
}
```
